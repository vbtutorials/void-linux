# QEMU/KVM

## Встановлення та налаштування
``` bash
sudo xbps-install -Su virt-manager libvirt qemu
sudo xbps-install -S virt-viewer

sudo xbps-install -S vsv
sudo vsv status virt


sudo usermod -aG kvm $USER
sudo usermod -aG libvirt $USER
modprobe kvm-intel
ln -s /etc/sv/libvirtd /var/service
ln -s /etc/sv/virtlockd /var/service
ln -s /etc/sv/virtlogd /var/service
```
## Зміна правил для доступу
/etc/polkit-1/rules.d/50-libvirt.rules 

``` bash
/* Allow users in kvm group to manage the libvirt
daemon without authentication */
polkit.addRule(function(action, subject) {
    if (action.id == "org.libvirt.unix.manage" &&
        subject.isInGroup("kvm")) {
            return polkit.Result.YES;
    }
});
```
/etc/polkit-1/rules.d/50-org.libvirt.unix.manage.rules
``` bash
polkit.addRule(function(action, subject) {
        if (action.id == "org.libvirt.unix.manage" &&
            subject.user == "$USER") {
                return polkit.Result.YES;
                polkit.log("action=" + action);
                polkit.log("subject=" + subject);
        }
});
```

## Джерела
[https://debuntu.ru/](https://debuntu.ru/n/void-linux-ustanovka-kvm)

[https://www.daveeddy.com/](https://www.daveeddy.com/2019/02/11/kvm-virtualization-with-virtmanager-on-void-linux/)

[https://medium.com/](https://medium.com/@leandroembu/virtualizando-com-virt-manager-no-void-linux-7c85e24edc46)

[https://www.reddit.com/r/voidlinux/](https://www.reddit.com/r/voidlinux/comments/ghwvv5/guide_how_to_setup_qemukvm_emulation_on_void_linux/)



