# Нотатки та посилання на ресурси

## Після встановлення:
* [https://debuntu.ru/a/void-linux-posleustanovochnye-shagi](https://debuntu.ru/a/void-linux-posleustanovochnye-shagi)
* [https://github.com/dawidpotocki/biual/wiki/English:-Installation](https://github.com/dawidpotocki/biual/wiki/English:-Installation)

## Неофіційні збірки Void Linux
* [https://repo.voidlinux.de/](https://repo.voidlinux.de/)
* [https://www.voidbuilds.xyz/](https://www.voidbuilds.xyz/)
* Збірки Void Linux від José Santos [(Budgie Desktop та інші)](https://www.youtube.com/watch?reload=9&v=SEb3nd3w3AM)
