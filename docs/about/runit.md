# RUNIT

!!! quote "Опис"
        **runit** - це набір інструментів, що включає в себе ідентифікатор PID 1, а також сумісну інфраструктуру контролю за процесами, а також утиліти, що оптимізують створення та обслуговування сервісів.
## Про
runit використовує концепцію каталогу служб, відповідального за окремий процес та необов'язковою службою журналу.

## Каталоги runit
* `/var/service` - завжди пов'язаний з поточним рівнем запуску
* `/etc/sv` - каталог, що містить сервісні файли, розміщені у xbps
* `/etc/runit/runvdir` - каталог, що містить усі доступні рівні запуску

## Файли runit
* `/etc/runit/1` - етап 1, завдання одноразової ініціалізації системи
* `/etc/runit/2` - етап 2, як правило, запускає runsvdir (не повинен повертатися, поки система не зупиниться або перезавантажиться)
* `/etc/runit/3` - етап 3, завдання вимкнення системи
* `/etc/runit/ctrlaltdel` - Runit виконає це під час отримання сигналу SIGINT
* `/etc/runit/runsvdir/*` - рівні зупуску
* `/etc/runit/sv/*` - каталог, що містить підкаталоги доступних службових файлів
* `/run/runit/service` - завжди зв'язаний з активним рівнем запуску (sv буде шукати тут запущену службу)

Однак оскільки сам runit залежить від runit-rc, додатково буде встановлено кілька додаткових файлів rc, більшість з яких міститься у /etc/rc та /usr/lib/rc. 


## Команди керування
* **sv** - утиліта runit для керування процесами
* **chpst** - маніпулятор середовища runit (контроль процесного середовища, включаючи обмеження пам’яті, обмеження ядер, сегментів даних, середовищ, привілеїв користувача/групи тощо)
* **runsv** - запускає та контролює службу та, можливо, службу журналу, що додається
* **svlogd** - утиліта ведення журналу runit (простий, але потужний реєстратор, включає автоматичне обертання, засноване на різних методах (час, розмір тощо), післяобробку, узгодження шаблонів та параметри сокета (віддалений журнал)).
* **runsvchdir** - перемикач рівнів запуску runit
* **runsvdir** - запускає та контролює набір процесів runv(8)
* **runit** - процес номер 1 

## Макет каталогу служби
Каталогу служби потрібний лише один файл - виконуваний файл з ім'ям run, який запускає процес. 
```bash
$ ls -al /var/service/iptables/
finish     run        supervise/
```
Вміст файлу run:
```bash
#!/bin/sh
[ ! -e /etc/iptables/iptables.rules ] && exit 0
iptables-restore -w 3 /etc/iptables/iptables.rules || exit 1
exec chpst -b iptables pause

```
Якщо каталог містить каталог з іменем log, канал буде відкритий на вхід процесу в каталог журналів.

## Основи використання

* **Увімкнення сервісу** (у runlevel за замовчуванням)

`# ln -s /etc/sv/<service_name> /var/service`

Якщо система наразі не працює, послугу можна підключити безпосередньо до runsvdir за замовчуванням:

`# ln -s /etc/sv/<service> /etc/runit/runsvdir/default/`

Щоб служба не запускалася під час завантаження, дозволяючи runit керувати нею, створіть файл, названий down в її каталозі:

`# touch /etc/sv/<service>/down`

* **Вимкнення сервісу**

`# rm /var/service/<service_name>`

З runsvdir за замовчуванням або якщо система не працює:

`# rm /etc/runit/runsvdir/default/<service>`

* **Тестування сервісу**

Щоб перевірити, чи служба працює правильно, коли її запустив адміністратор, слід запустити її один раз, перш ніж повністю активувати її:

``` bash
# touch /etc/sv/<service>/down
# ln -s /etc/sv/<service> /var/service
# sv once <service>
```

* **Команди start, stop, restart та status**
``` bash
# sv up <services>
# sv down <services>
# sv restart <services>
# sv status <services>
```
Заповнювачем <services> може бути:

- Назва служби (назва каталогу служби) у /var/service

- Повний шлях до послуги

Наприклад, наступні команди показують стан певної послуги та всіх увімкнених служб:

``` bash 
# sv status dhcpcd
# sv status /var/service/*
```

## Рівні запуску

За замовчуванням у runit є 2 рівні запуску: **default** та **single**. 

- Рівень запуску `single` призначений для використання при налагодженні чи однокористувацькому запуску. Він запускає sulogin(8) за замовчуванням.

- Рівень `default` запустить agetty(8) та деякі інші сервіси (у багатокористувацькому режимі)


Для активації власного рівня запуску, необхідно просто створити новий каталог в `/etc/runit/runsvdir/` і злінкувати потрібні послуги:

``` bash
# cp -a /etc/runit/runsvdir/default /etc/runit/runsvdir/my_runlevel
# rm /etc/runit/runsvdir/my_runlevel/agetty-tty[3456] # remove all gettys except for tty1 and tty2
# ln -s /etc/sv/dcron /etc/runit/runsvdir/my_runlevel/ # add the cron service
```

**Зміна рівня запуску**

```# runsvchdir my_runlevel ```

(це зупинить усі служби, які зараз запущені, і запустить усі служби в новому рівні)

Щоб зробити цей рівень запуску за замовчуванням, потрібно додати його до командного рядка завантажувача.
Для GRUB відредагуйте `/etc/default/grub` додавши:

`GRUB_CMDLINE_LINUX_DEFAULT="loglevel=4 my_runlevel"`

Після цього необхідно виконати процедуру **update-grub**.

`sudo grub-mkconfig -o /boot/grub/grub.cfg`



## Джерела
* Сторінка документації по runit [https://docs.voidlinux.org/config/services/index.html](https://docs.voidlinux.org/config/services/index.html)

* [https://wiki.artixlinux.org/Main/Runit](https://wiki.artixlinux.org/Main/Runit)

* Базове керування сервісами runit (by  Matheus Augusto da Silva) [дивитись](https://www.youtube.com/watch?v=PRpcqj9QR68)

* Сторінка документації [http://smarden.org/runit/](http://smarden.org/runit/)