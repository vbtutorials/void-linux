# Способи інсталяції програм у Void Linux

## Встановлення програм з репозиторіїв

У загальному випадку встановлення програм виконують із офіційних репозиторіїв у мережі інтернет командою:
``` bash
$ sudo xbps-install {pkgname}
```

Або командою утиліт пакету xtools:
```bash
$ xi {pkgname}
```

!!! warning "Увага"
    Детальний опис команд для роботи з пакетами знаходиться у розділі - [Пакетний менеджер XBPS](../../about/xbps/)



## Збірка пакетів програм
Використовується репозиторій **void-packages** (XBPS source packages collection)

``` bash 
git clone --recursive https://github.com/void-linux/void-packages
cd void-packages
echo 'XBPS_ALLOW_RESTRICTED=yes' > etc/conf
./xbps-src  binary-bootstrap
./xbps-src pkg {pkgname}
xbps-install -Sy -R hostdir/binpkgs {pkgname}
```

Для перегляду списку доступних пакетів цього репозиторію:
``` bash
grep -rl '^restricted=' srcpkgs/
```


## Створення локального репозиторію 

Збудовані власноруч пакети у форматі *.xbps чи завантажені з неофіційних сайтів можна встановити створивши локальний репозиторій:

``` bash
xbps-rindex -a *.xbps && xbps-install --repository=$PWD {pkgname}
```
