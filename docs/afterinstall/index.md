# Налаштування після встановлення

!!! warning "Зверніть увагу"
    У довіднику описується налаштування після встановлення мінімальної системи без графічної оболонки та додаткових утиліт. Для графічного середовища використовуються тайловий віконний менеджер i3-wm (або spectrwm). Скачати цей образ можна з офіційного дзеркала: [https://alpha.de.repo.voidlinux.org/live/current/](https://alpha.de.repo.voidlinux.org/live/current/)

![alt](../images/void_back.png "Void Linux with i3wm Desktop")

## Оновлення системи

``` bash
sudo xbps-install -Su
sudo reboot
```
**Встановлення редактора та автодоповнення**

``` bash
sudo xbps-install vim bash-completion 
```

## Локалізація
Якщо мова локалізації була вибрана на етапі встановлення, то у системі уже будуть основні налаштування. Їх потрібно перевірити і при необхідності сконфігурувати нові:

``` bash
locale -a
```
Файли з налаштуваннями мови локалізації:
``` bash
sudo vim /etc/default/libc-locales 
sudo vim /etc/locale.conf 
```
!!! success "Перевірте наявність у файлах розкоментованих рядків"
    uk_UA.UTF-8

    LANG=uk_UA.UTF-8

Конфігурування при зміні налаштувань:
``` bash
sudo xbps-reconfigure -f glibc-locales
```

## Зміна налаштувань завантажувача
``` bash
sudo vim /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg 
```

## Базова графічна система
``` bash
sudo xbps-install -S xorg-minimal xorg-fonts xorg-apps xorg-input-drivers 
```

**Драйвери та утиліти**
``` bash
sudo xbps-install -S xf86-video-amdgpu arandr light linux-firmware linux-firmware-amd
```
!!! warning "Зверніть увагу"
        Void має комплексний пакет ** xorg** , який встановлює сервер та всі безкоштовні драйвери відео, драйвери введення, шрифти та базові програми.

## Мультимедіа
``` bash
sudo xbps-install -S pulseaudio alsa-plugins-pulseaudio pavucontrol pamixer
```

## Системні налаштування
``` bash
vim /ets/hosts
# додати запис
127.0.1.1	voidbook.localdomain	voidbook
```
**Встановлення та запуск системних служб**
``` bash
sudo xbps-install -S base-devel dbus elogind  gnupg2 pinentry-gtk polkit polkit-gnome
sudo ln -srf /etc/sv/{dbus,polkitd,elogind} /var/service
sudo xbps-install -S lm_sensors sysstat
sudo ln -s /etc/sv/fancontrol /var/service/
sudo updatedb
```

## Додакові репозиторії
``` bash
sudo xbps-install -S void-repo-nonfree
# для підтримки х32 програм
sudo xbps-install -S void-repo-multilib void-repo-multilib-nonfree
```

## Налаштування мережі
``` bash
ip link show
sudo xbps-install -S NetworkManager network-manager-applet net-tools
ping google.com
sudo ln -s /etc/sv/NetworkManager/ /var/service/
```

**Встановлення з'єднання за допомогою nmcli:**
``` bash
nmcli d
nmcli r wifi on
nmcli d wifi list
sudo nmcli d wifi connect $WIFINETWORK password $PASS
# для видалення
nmcli connection delete $WIFINETWORK
``` 

## Віконний менеджер
``` bash
# для встановлення i3wm
sudo xbps-install -S i3-gaps i3status dmenu networkmanager-dmenu
# для встановлення spectrwm
sudo xbps-install -S spectrwm dmenu xlockmore
```

## Менеджер дисплеїв
``` bash
sudo xbps-install -S lightdm lightdm-gtk-greeter
sudo ln -s /etc/sc/lightdm /var/service
```
!!! tip "Додаткова інформація:"
    Налаштування екрану входу lightdm-gtk-greeter [geekkies.in.ua](https://geekkies.in.ua/linux/nastrojka-ekrana-vhoda-lightdm-gtk-greeter.html)

    Стаття ArchWiki про LightDM [wiki.archlinux.org](https://wiki.archlinux.org/index.php/LightDM)

## Встановлення додатково ПЗ
``` bash
# програми та утиліти для коректної роботи i3wm
sudo xbps-install -S st inxi htop mc dialog mlocate curl neofetch setxkbmap xclip rofi compton scrot mpv ffmpeg 
# графічні програми та додаткові утиліти
sudo xbps-install -S pcmanfm feh unzip xev galculator xbacklight xdg-utils firefox-esr vscode gvim 
```

**Утиліти для автомонтування USB-накопичувачів**
``` bash
sudo xbps-install -S gvfs gvfs-mtp file-roller jmtpfs gamin udisks
```

## Теми оформлення та шрифти
``` bash
sudo xbps-install -S arc-theme arc-icon-theme lxappearance 
sudo xbps-install -S ttf-ubuntu-font-family freefont-ttf dejavu-fonts-ttf font-awesome5 font-fira-ttf font-ibm-plex-ttf font-symbola fonts-roboto-ttf noto-fonts-ttf
```
!!! success "Шрифти можна встановити шляхом копіювання їх у директорію:"
    sudo cp fontawesome-webfont.ttf /usr/share/fonts/TTF/ && fc-cache -f -v


## Встановлення та видалення ядер linux
``` bash
sudo xbps-install -S linux5.6 linux5.6-headers
sudo xbps-remove -F linux5.4 linux5.4-headers
sudo grub-mkconfig -o /boot/grub/grub.cfg
sudo vkpurge list
sudo vkpurge rm all
```
## Очищення системи
```bash
# видалення пакетного кешу
xbps-remove -yO
# видалення згублених пакетів
xbps-remove -yo
# видалення старих ядер
vkpurge rm all
```




