# Встановлення прикладних програм
!!! warning "Увага"
    Перед встановленням слід ознайомитись з розділами [Способи інсталяції ПЗ](../../about/soft_install/) та [Пакетний менеджер XBPS](../../about/xbps/)

### WPS Office
Для встановлення пакету, його можна збудувати з репозиторію void-packages або скачати з неофіційних дзеркал:
``` bash
git clone --depth 1 https://github.com/void-linux/void-packages/
cd void-packages
echo XBPS_ALLOW_RESTRICTED=yes > etc/conf
./xbps-src binary-bootstrap
./xbps-src pkg wps-office
xbps-install -Sy -R hostdir/binpkgs wps-office
```

!!! tip "Джерело"
    Форум void linux [reddit.com](https://www.reddit.com/r/voidlinux/comments/gbn22f/how_to_convert_a_deb_package/)

### Шрифти сумісні з ms-fonts

Times New Roman, Arial та інші подібні шрифти належать Microsoft, тому вони не постачаються з дистрибутивами GNU/Linux, щоб уникнути проблем з ліцензіями.

Для основних потреб часто використовують шрифти з відкритим кодом "Liberation fonts", щоб замінити аналоги від Microsoft. Для встановлення використовується команда:

```bash
sudo xbps-install -S liberation-fonts-ttf
```
У дистрибутиві Void Linux існує можливість встановлення і сторонніх шрифтів. Вони зібрані у пакеті "msttcorefonts".

``` bash
git clone https://github.com/void-linux/void-packages
cd void-packages
./xbps-src binary-bootstrap
echo "XBPS_ALLOW_RESTRICTED=yes" >> etc/conf
./xbps-src pkg -f msttcorefonts
xi msttcorefonts
```

### Firewall
* Базове налаштування для iptables (by Matheus Augusto da Silva) [дивитись](https://youtu.be/2_rzkNtYVGw)

* [https://wiki.voidlinux.org/Firewall_Configuration](https://wiki.voidlinux.org/Firewall_Configuration)


### i3
* Офіційна документація [i3wm.org](https://i3wm.org/docs/userguide.html)

* Відео-огляд i3wm: Jump Start [youtube.com](https://youtu.be/j1I63wGcvU4?list=PL5ze0DjYv5DbCv9vNEzFmP6sU7ZmkGzcf)

* Налаштування i3wm-gaps [youtube.com](https://www.youtube.com/watch?v=kQT11RANDG8)



### spectrwm
Відео-огляд та налаштування віконного менеджера spectrwm [youtube.com](https://youtu.be/v3FsgPk8WW4)


### WIRE massenger
``` bash
sudo xbps-install wire-desktop wireguard wireguard-tools 
```