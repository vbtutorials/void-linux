# Компіляція додаткових драйверів
**На прикладі безпровідної мережної карти rtl8723de:** 

!!! warning "Увага"
    Збірка драйверу можлива кількома способами. Переконайтеся, що встановлено пакети base-devel, linux{version}-headers та git. Якщо хочете використовувати dkms для створення та встановлення драйверу, переконайтеся, що пакет dkms також встановлений.

``` bash
git clone -b extended --single-branch https://github.com/lwfinger/rtlwifi_new.git
cd rtlwifi_new/
```
Тепер потрібно виконати:

``` bash
make
sudo make install
sudo /bin/sh -c 'echo "options rtl8723de ant_sel=2" >> /etc/modprobe.d/rtl8723de.conf'
sudo reboot
```
Або можна використати dkms для збудування та управління модулями:

``` bash
sudo dkms add ../rtlwifi_new
sudo dkms build rtlwifi-new/0.6 
sudo dkms install rtlwifi-new/0.6
sudo modprobe -v rtl8723de ant_sel=2
sudo reboot
```

!!! tip "Джерело:"
    Форум [https://h30434.www3.hp.com](https://h30434.www3.hp.com/t5/Notebook-Wireless-and-Networking/Realtek-8723DE-wifi-module-amp-Bluetooth-Linux-driver/td-p/6477307)