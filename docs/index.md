# Вітаю вас у довіднику операційної системи
![alt](images/void_linux.jpg "Void Linux")

**Void Linux** - це незалежна дистрибутивна версія операційної системи GNU/Linux, що базуєтся на бінарній пакетній системі **xbps** та системі ініціалізації **runit**.

**Джерела:**

* Офіційний сайт проекту [voidlinux.org](https://voidlinux.org/)

* Для перегляду офіційної документації відвідайте [docs.voidlinux.org](https://docs.voidlinux.org/)

* Сторінка форуму на [reddit.com](https://www.reddit.com/r/voidlinux/)

!!! question "Для чого це?"
    Ресурс розроблений для накопичення матеріалу [автора](https://gitlab.com/vborys) про дистрибутив українською мовою та підтримки проекту [voidlinux.org](https://voidlinux.org/)