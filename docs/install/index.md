# Інсталяція операційної системи Void Linux

Процес встановлення виконується у відповідному меню програми void-installer:

![alt](../images/install_menu.png "Void Linux")

## Встановлення на диск з MBR
* Інсталяція void linux+xfce4 (by Matheus Augusto da Silva) [дивитись](https://youtu.be/i3qShxSZn58)

## Встановлення на диск з UEFI:
* Інсталяція void linux+dwm (by Terminal Root) [дивитись](https://youtu.be/wzh_dKh3eps)





